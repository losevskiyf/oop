package oop;

import java.util.Arrays;

public class User {

    private String login;
    private char[] password;
    private Basket basket = new Basket(new Product[]{});

    public User(){}
    public User(String login, char[] password, Basket basket) {
        this.login = login;
        this.password = password;
        this.basket = basket;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public char[] getPassword() {
        return password;
    }

    public void setPassword(char[] password) {
        this.password = password;
    }

    public Basket getBasket() {
        return basket;
    }

    public void setBasket(Basket basket) {
        this.basket = basket;
    }

    @Override
    public String toString() {
        return "User{" +
                "login='" + login + '\'' +
                ", password=" + Arrays.toString(password) +
                ", basket=" + basket +
                '}';
    }

}