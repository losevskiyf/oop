package oop;

public class OOPApp {

    public static void main(String[] args) {

        Product bread = new Product("Bread", 30, 4.6f);
        Product meat = new Product("Meat", 300, 3.0f);
        Product milk = new Product("Milk", 90, 4.9f);

        Category food = new Category("Food", new Product[]{bread, meat, milk});

        Product dress = new Product("Dress", 4000, 4.4f);
        Product tie = new Product("Tie", 400, 4.9f);
        Product socks = new Product("Socks", 600, 3.2f);

        Category clothes = new Category("Clothes", new Product[]{dress, tie, socks});

        Product soap = new Product("Soap", 300, 4.7f);
        Product shavingFoam = new Product("Shaving Foam", 600, 5.0f);
        Product toothpaste = new Product("Toothpaste", 400, 4.0f);

        Category households = new Category("Households", new Product[]{soap, shavingFoam, toothpaste});

        Basket basket = new Basket(new Product[]{meat, tie, socks, shavingFoam});
        User realMan = new User("realman2009", "realman2009".toCharArray(), basket);

        System.out.println(realMan);

    }

}
